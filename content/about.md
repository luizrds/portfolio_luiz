---
title: 'about'
date: 2024-10-13T02:50:11-07:00
draft: false
layout: 'about'
---

Bem-vindo(a) ao meu portfolio pessoal! Eu sou Luiz Reis, estudante de engenharia mecatrônica @ POLI-USP, Product Manager e desenvolvedor back-end @ Poli Júnior. Tenho forte interesse em buscar soluções criativas na área, tendo como missão pessoal causar resultados impactantes através de minhas ações. As principais tecnologias, frameworks e afins utilizados em meus projetos são: TypeScript, Node.js, Docker, Prisma e DigitalOcean Droplets.