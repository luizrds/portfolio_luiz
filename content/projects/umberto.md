---
title: 'Umberto'
draft: false
imagePath: "images/ecology.svg"
techList:
    - tech: "TypeScript"
    - tech: "AWS"
    - tech: "Prisma"
    - tech: "jsonwebtoken"
    - tech: "Docker"
    - tech: "DigitalOcean"
---

Umberto é um projeto que relaciona ecologia e clube de benefícios. Por ser um projeto ainda em execução, esse é o limite de informações públicas.