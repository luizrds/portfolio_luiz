---
title: 'Saf'
draft: false
imagePath: "images/grave.svg"
techList:
    - tech: "TypeScript"
    - tech: "AWS"
    - tech: "PostgreSQL"
    - tech: "jsonwebtoken"
    - tech: "Docker"
    - tech: "DigitalOcean"
    - tech: "mailjet"
---

Saf é um projeto que relaciona o conceito de clube de benefícios associado a planos funerários, sendo isso tudo que posso falar sobre o projeto.